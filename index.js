require('dotenv').config();
const moment = require('moment');
const _ = require('lodash');
const { Client } = require("@notionhq/client")

const notion = new Client({
    auth: process.env.NOTION_TOKEN,
})

const DATE_FORMAT = 'YYYY-MM-DD';
const START_OCT = moment().startOf('month').month(9);
const END_MARCH = moment().endOf('month').month(2);

const SCHEDULER_OPTIONS = {
    SETTIMANA_SUCCESSIVA: async(p) => {
        let { start, end } = p.properties.Date.date;
        start = moment(start).add(7, 'days').format(DATE_FORMAT);
        end = moment(end).add(7, 'days').format(DATE_FORMAT);
        return { start, end };
    },
    ANNO_SUCCESSIVO: async(p) => {
        let { start, end } = p.properties.Date.date;
        start = moment(start).add(1, 'year').format(DATE_FORMAT);
        end = moment(end).add(1, 'year').format(DATE_FORMAT);
        return { start, end };
    },
    MESE_SUCCESSIVO: async(p) => {
        let { start, end } = p.properties.Date.date;
        start = moment(start).add(1, 'month').format(DATE_FORMAT);
        end = moment(end).add(1, 'month').format(DATE_FORMAT);
        return { start, end };
    },
    DUE_MESI_DOPO: async(p) => {
        let { start, end } = p.properties.Date.date;
        start = moment(start).add(2, 'month').format(DATE_FORMAT);
        end = moment(end).add(2, 'month').format(DATE_FORMAT);
        return { start, end };
    },
    TRE_MESI_DOPO: async(p) => {
        let { start, end } = p.properties.Date.date;
        start = moment(start).add(3, 'month').format(DATE_FORMAT);
        end = moment(end).add(3, 'month').format(DATE_FORMAT);
        return { start, end };
    },
    DUE_SETTIMANE: async(p) => {
        let { start, end } = p.properties.Date.date;
        start = moment(start).add(2, 'week').format(DATE_FORMAT);
        end = moment(end).add(2, 'week').format(DATE_FORMAT);
        return { start, end };
    },
    LENZUOLA_SCHEDULE: async(p) => {
        let { start, end } = p.properties.Date.date;
        if (moment().isBetween(END_MARCH, START_OCT)) {
            // Summer
            start = moment(start).add(1, 'month').format(DATE_FORMAT);
            end = moment(start).add(1, 'month').format(DATE_FORMAT);
        } else {
            // Winter
            start = moment(start).add(6, 'week').format(DATE_FORMAT);
            end = moment(start).add(6, 'week').format(DATE_FORMAT);
        }
        return { start, end };
    },
    DANA_SCHEDULE: async(p) => {
        let { start, end } = p.properties.Date.date;
        if (moment().isBetween(END_MARCH, START_OCT)) {
            // Summer
            start = moment(start).add(2, 'month').format(DATE_FORMAT);
            end = moment(start).add(2, 'month').format(DATE_FORMAT);
        } else {
            // Winter
            start = moment(start).add(3, 'month').format(DATE_FORMAT);
            end = moment(start).add(3, 'month').format(DATE_FORMAT);
        }
        return { start, end };
    }
}


async function handlePage(page) {
    const nextDate = await SCHEDULER_OPTIONS[page.properties.Next.select.name](page)
    console.log(nextDate);
    console.log(page);

    const duplicate = _.omit(page, ['object', 'created_time', 'id', 'last_edited_time', 'archived', 'url']);
    duplicate.properties.Date.date = nextDate;
    delete duplicate.properties.Active

    const result = await notion.pages.create(duplicate);
    console.log(result);
    // Set a boolean to prevent being fetched again
    await notion.pages.update({
        page_id: page.id,
        properties: {
            NextReady: {
                checkbox: true
            }
        }
    })
}


async function run() {
    const pages = await notion.databases.query({
        database_id: process.env.NOTION_DB,
        filter: {
            and: [{
                    property: 'Active',
                    checkbox: {
                        equals: true
                    }
                },
                {
                    property: 'NextReady',
                    checkbox: {
                        equals: false,
                    }
                },
                {
                    property: 'Next',
                    select: {
                        is_not_empty: true,
                    }
                }
            ]
        }
    });

    for (let page of pages.results) {
        await handlePage(page);
    }
}

run();